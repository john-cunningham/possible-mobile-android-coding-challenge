package cunningham.john.androidcodingchallenge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN_ACTIVITY";
    private static final String JSON_URL = "http://de-coding-test.s3.amazonaws.com/books.json";
    @SuppressWarnings("FieldCanBeLocal")
    private RequestQueue requestQueue;
    private final List<Book> books = new ArrayList<>();
    private BookAdapter bookAdapter;
    private ProgressBar progressBar;

    /**
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        // RecyclerView setup
        //
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        bookAdapter = new BookAdapter(this, books);
        recyclerView.setAdapter(bookAdapter);

        //
        // ProgressBar setup
        //
        progressBar = findViewById(R.id.progressBar);

        //
        // Networking setup
        //
        requestQueue = Volley.newRequestQueue(this);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, JSON_URL, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        parseData(response);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.getMessage());
                    }
                });

        requestQueue.add(request);
    }

    /**
     * Parse the JSON response into Book objects for the adapter
     * @param data JSONArray
     */
    private void parseData(JSONArray data) {

        if (data != null) {
            for (int i = 0; i < data.length(); i++) {
                try {
                    JSONObject bookJson = data.getJSONObject(i);
                    String title = bookJson.getString("title");
                    String imageUrl = bookJson.getString("imageURL");
                    Book book = new Book(title, imageUrl);
                    books.add(book);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
            bookAdapter.notifyDataSetChanged();
        }
        progressBar.setVisibility(View.GONE);
    }
}
