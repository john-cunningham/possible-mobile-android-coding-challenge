package cunningham.john.androidcodingchallenge;

class Book {

    private final String title;
    private final String imageUrl;

    Book(String title, String imageUrl) {
        this.title = title;
        this.imageUrl = imageUrl;
    }

    String getTitle() {
        return title;
    }

    String getImageUrl() {
        return imageUrl;
    }
}